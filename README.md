# FriendList

## Setup

Just run the damn thing:

```
docker-compose up
```

Developer mode (same thing but mounted local codebase and file watchers):

```
docker-compose up -f docker-compose-dev.yml
```

Webserver: http://localhost:8080/

API: http://localhost:3000/

DB: postgres://user:pass@localhost:5432/db

## API

### Creating User

```POST http://localhost:3000/user```

With a POST body of the following structure:

```
{
    name: String
    email: String
    image: String
}
```

### Creating Friends

```POST http://localhost:3000/user/:userId/:friendId```

UserId and friendId being the Ids of existing Users.

Or if that doesn't work:

```Water (35 L), Carbon (20 kg), Ammonia (4 L), Lime (1.5 kg), Phosphorous (800 g), Salt (250 g), Saltpeter (100 g), Sulfur (80 g), Fluorine (7.5 g), Iron (5 g), Silicon (3 g) and fifteen traces of other elements.```

Also a bit of chalk...
