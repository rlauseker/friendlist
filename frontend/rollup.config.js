import ts from "@wessberg/rollup-plugin-ts";
import resolve from '@rollup/plugin-node-resolve';
import scss from 'rollup-plugin-scss';

export default {
    input: './src/index.ts',
    output: {
        file: './dist/index.js',
        format: 'cjs'
    },
    plugins: [
        ts({
            tsconfig: "./tsconfig.json"
        }),
        scss({
            output: true,
        }),
        resolve()
    ]
};
