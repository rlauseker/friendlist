import {config} from '../config'
import {User} from './User'

export interface Friend {
    userId: Number
    friendId: Number
}

export class FriendFacade {
    private static url = new URL(config.userServiceUrl + '/friend/')

    public static async getFriendsByUserId(id: Number): Promise<Array<User>> {
        const response = await fetch(FriendFacade.url.toString() + id)

        return await response.json()
    }

    public static async getFriendsByUserIdAndName(id: Number, name: String): Promise<Array<User>> {
        const response = await fetch(FriendFacade.url.toString() + id + "/" + name)

        return await response.json()
    }
}
