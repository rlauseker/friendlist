import {config} from '../config'

export interface User {
    id: Number
    name: String
    email: String
    image: String
}

export class UserFacade {
    private static url = new URL(config.userServiceUrl + '/user/')

    public static async getAllUsers(): Promise<Array<User>> {
        const response = await fetch(UserFacade.url.toString())

        return await response.json();
    }

    public static async getUserByName(name: String): Promise<Array<User>> {
        const response = await fetch(UserFacade.url.toString() + name)

        return await response.json();
    }
}
