import {LitElement, css, html, PropertyValues} from 'lit-element'
import {grid} from '../style/simple-grid'
import {Search, SearchLabels} from './Search'
import {User} from '../model/User'
import {FriendFacade} from '../model/Friend'

export class FriendsList extends LitElement {
    static is = 'fl-friend-list'

    private searchValue: String
    private userId: Number

    private list: Array<User> = []

    private searchModule: Search

    private labels: SearchLabels = {
        userLabel: 'Select your current user',
        searchLabel: 'Search for a name',
    }

    static get properties() {
        return {
            searchValue: {type: String},
            list: {type: Array}
        }
    }

    constructor() {
        super()
    }

    async firstUpdated(changedProperties: PropertyValues) {
        this.searchModule = this.shadowRoot.querySelector(Search.is)
        this.searchModule.addEventListener(Search.EVENT_SEARCH_FRIEND, this.onSearch.bind(this))
        this.searchModule.addEventListener(Search.EVENT_CHANGE_USER, this.onChangeUser.bind(this))
    }

    private async onChangeUser(event: CustomEvent) {
        this.userId = event.detail.message
        this.list = await this.setFriendsOfCurrentUser(this.userId)
    }

    private async setFriendsOfCurrentUser(userId: Number): Promise<Array<User>> {
        return await FriendFacade.getFriendsByUserId(userId)
    }

    private async onSearch(event: CustomEvent) {
        this.searchValue = event.detail.message
        this.list = await this.searchForFriends(this.userId, this.searchValue)
    }

    private async searchForFriends(userId: Number, name: String): Promise<Array<User>> {
        return await FriendFacade.getFriendsByUserIdAndName(userId, name)
    }

    render() {
        return html`
          <div class="box">
            <div class="row">
              <div class="search col-xs-12 col-md-4">
                Here you can search in your Friendlist:
                <br/>
                <br/>
                <fl-search labels="${JSON.stringify(this.labels)}"></fl-search>
              </div>    
              <div class="list col-xs-12 col-md-8">
                <div class="searchValue">
                  Searchterm: ${this.searchValue || '--'}
                </div>
                <div class="listItems">
                  ${this.list.map(i => html`<fl-friend-entry user="${JSON.stringify(i)}"></fl-friend-entry>`)}
                </div>
              </div>
            </div>
          </div> 
        `
    }

    static get styles() {
        // Including the grid here a second time is one of the drawbacks of WebComponents.
        // Payload: +
        // Stability: +++
        return [
            grid,
            css`
              .box {
                padding: 0 1rem;
                margin: 1rem;
              }
              
              .search {
                padding: 0.5rem;
                background-color: #E9EEFC;
                height: fit-content;
              }
              
              .list {
                padding: 0.5rem 0;
                background-color: #fff;
              }
                                          
              .list fl-friend-entry {
                display: block;
                margin-bottom: 10px;
              }
              
              .searchValue {
                margin-bottom: 10px;
              }
              
              @media only screen and (min-width: 48em) {
                .box {
                  filter: drop-shadow(8px 8px 6px #ddd);
                }
              }
              
              @media only screen and (min-width: 62em) {
                .list {
                  padding: 0.5rem;
                }
                
                .search {
                  height: auto;
                }
                
                .box > .row {
                  min-height: 30rem;
                }
              }
            `
        ]
    }
}
