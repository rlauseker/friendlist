import {LitElement, css, html} from 'lit-element'
import {debounce} from 'ts-debounce/dist/src/index'
import {User, UserFacade} from '../model/User'

export interface SearchLabels{
    userLabel: String
    searchLabel: String
}

export class Search extends LitElement {
    static is = 'fl-search'

    static EVENT_CHANGE_USER = 'changeUser'
    static EVENT_SEARCH_FRIEND = 'searchFriend'

    public userId: String
    public searchQuery: String
    public labels: SearchLabels
    public userList: Array<User> = []

    constructor() {
        super()

        this.userId = ''
        this.searchQuery = ''

        UserFacade.getAllUsers().then((userList: User[]) => {
            this.userList = userList
        })
    }

    static get properties() {
        return {
            labels: {type: Object},
            userList: {type: Array}
        }
    }

    private handleChangeCurrentUser(event: Event) {
        const input: HTMLInputElement = event.target as HTMLInputElement

        this.userId = input.value

        let newEvent = new CustomEvent(Search.EVENT_CHANGE_USER, {
            detail: {
                message: this.userId
            }
        })

        this.dispatchEvent(newEvent)
    }

    private handleSearchFriend(event: Event) {
        const input: HTMLInputElement = event.target as HTMLInputElement

        this.searchQuery = input.value

        debounce(() => {
            let newEvent = new CustomEvent(Search.EVENT_SEARCH_FRIEND, {
                detail: {
                    message: this.searchQuery
                }
            })

            this.dispatchEvent(newEvent)
        }, 250, {isImmediate: false})()
    }


    render() {
        return html`
          <div>
            <select @input="${this.handleChangeCurrentUser}">
              <option selected class="default">${this.labels.userLabel}</option>
              ${this.userList.map(user => html`<option value="${JSON.stringify(user.id)}">${user.name}</option>`)}
            </select>
          </div> 
          <div>
            <input placeholder="${this.labels.searchLabel}" @input="${this.handleSearchFriend}" />
          </div> 
        `
    }

    static get styles() {
        return css`
          input, select {
            padding: 5px;
            width: 80%;
            border: 0px;
            background-color: #fff;
            margin-bottom: 10px;
            box-sizing: content-box;
          }
          
          input::placeholder {
            color: #bbb;
          }
        `
    }
}
