import {LitElement, css, html} from 'lit-element'
import {User} from '../model/User'

export class FriendEntry extends LitElement {
    static is = 'fl-friend-entry'

    static get properties() {
        return {
            user: {type: Object}
        }
    }

    private user: User

    static get styles() {
        return css`
          .box {
            height: 50px;
            padding: 0 10px 0 60px;
            background-color: #eee;
            background-repeat: no-repeat;
            background-position: left;
            line-height: 25px;
            vertical-align: middle;
          }
          
          .box:hover {
            background-color: #ddd;
            cursor: pointer;
          }
        `
    }

    render() {
        return html`
          <div class="box" style="background-image: url(${this.user.image})">
            <div>${this.user.name}</div>
            <div><a href="mailto:${this.user.email}">Mail</a></div> 
          </div> 
        `
    }
}
