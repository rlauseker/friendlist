import './style/index.scss'

import {Search} from './component/Search'
import {FriendEntry} from './component/FriendEntry'
import {FriendsList} from './component/FriendsList'

// WebComponents go brrrrr... :D
customElements.define(Search.is, Search)
customElements.define(FriendEntry.is, FriendEntry)
customElements.define(FriendsList.is, FriendsList)
