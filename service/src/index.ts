import {NextFunction, Request, Response} from 'express'
import * as core from 'express-serve-static-core'
import {createAndPopulateUserTable} from './entities/user/user.controller'
import {userRoutes} from './entities/user/user.routes'
import {createAndPopulateFriendTable} from './entities/friend/friend.controller'
import {friendRoutes} from './entities/friend/friend.routes'
const express = require("express");
const bodyParser = require("body-parser");


const log = require("../config/logger.ts").log;

const app: core.Express = express();
app.use(bodyParser.urlencoded({limit: "10mb", extended: true}));
app.use(bodyParser.json({limit: "10mb", extended: true}));

app.use(function(_req: Request, res: Response, next: NextFunction) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
    next();
});

app.get("/", (_req: Request, res: Response) => {
    res.json({"message": "Welcome to Ceres-API."});
});

userRoutes(app)
friendRoutes(app)

const port: number = parseInt(process.env.PORT || '', 10) || 3000

app.listen(port, () => {
    log.info("Server is listening on port " + port);
});

// Just to ensure some data is available
createAndPopulateUserTable()
createAndPopulateFriendTable()
