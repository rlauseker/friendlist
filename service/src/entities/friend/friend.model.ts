import {DataTypes, Model} from 'sequelize'
import {sequelize} from '../../database'

export class Friend extends Model {
    userId: number
    friendId: number
}

Friend.init({
    userId: DataTypes.INTEGER,
    friendId: DataTypes.INTEGER,
}, {sequelize, modelName: 'friend'})
