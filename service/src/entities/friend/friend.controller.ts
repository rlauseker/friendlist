import {Request, Response} from 'express'
import {Friend} from './friend.model'
import {sequelize} from '../../database'
import {Op} from 'sequelize'
import {User} from '../user/user.model'

// Create and Save a new User
export async function create(req: Request, res: Response) {
    // Validate request
    if (!req.params.userId || !req.params.friendId) {
        return res.status(400).send({
            message: 'User or friend id can not be empty'
        })
    }

    // Save User in the database
    const user = await Friend.create({
        userId: req.params.userId,
        friendId: req.params.friendId
    })

    return res.send(user)
}

// Retrieve and return all friend from the database.
export async function findAll(_req: Request, res: Response) {
    const friends = await Friend.findAll()

    if (friends.length == 0) {
        return res.status(404).send({
            message: 'friend not found'
        })
    }

    return res.send(friends)
}

// Find a friends of user with a userId
export async function findById(req: Request, res: Response) {
    const friends = await Friend.findAll({where: {userId: req.params.userId}})

    if (!friends) {
        return res.status(404).send({
            message: 'No Friends found not found'
        })
    }

    const friendIds: number[] = friends.map((friend: Friend) => {
        return friend.friendId
    })

    // Doing two queries is ugly and bad performance, should just write the query manually...
    // @todo learn ORM or write query manually ^^'
    const users = await User.findAll({
        where: {
            id: {
                [Op.in]: friendIds
            }
        }
    })

    return res.send(users)
}

// Find a friends of user with a userId and name
export async function findByIdAndName(req: Request, res: Response) {
    const friends = await Friend.findAll({where: {userId: req.params.userId}})

    if (!friends) {
        return res.status(404).send({
            message: 'No Friends found not found'
        })
    }

    const friendIds: number[] = friends.map((friend: Friend) => {
        return friend.friendId
    })

    // Doing two queries is ugly and bad performance, should just write the query manually...
    // @todo learn ORM or write query manually ^^'
    const users = await User.findAll({
        where: {
            id: {
                [Op.in]: friendIds
            },
            name: {
                [Op.like]: '%' + req.params.name + '%'
            }
        }
    })

    return res.send(users)
}

export async function createAndPopulateFriendTable() {
    await Friend.sync()
    const friends = await Friend.findAll()

    // Dummy data
    if (friends.length == 0) {
        await sequelize.sync()

        Friend.create({
            userId: 1,
            friendId: 2
        })
        Friend.create({
            userId: 1,
            friendId: 3
        })
        Friend.create({
            userId: 1,
            friendId: 4
        })
        Friend.create({
            userId: 1,
            friendId: 5
        })
        Friend.create({
            userId: 1,
            friendId: 6
        })
        Friend.create({
            userId: 2,
            friendId: 1
        })
        Friend.create({
            userId: 3,
            friendId: 5
        })
        Friend.create({
            userId: 3,
            friendId: 6
        })
        Friend.create({
            userId: 4,
            friendId: 6
        })
        Friend.create({
            userId: 4,
            friendId: 1
        })
        Friend.create({
            userId: 5,
            friendId: 2
        })
    }
}
