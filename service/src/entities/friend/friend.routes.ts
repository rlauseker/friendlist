import * as core from 'express-serve-static-core'

export function friendRoutes(app: core.Express) {
    const friends = require('./friend.controller.ts')

    // Create a new User
    app.post('/friend/:userId/:friendId', friends.create)

    // Retrieve a single User with userId
    app.get('/friend/:userId', friends.findById)

    // Retrieve a single User with userId
    app.get('/friend/:userId/:name', friends.findByIdAndName)
}
