import * as core from 'express-serve-static-core'

export function userRoutes(app: core.Express) {
    const users = require('./user.controller.ts')

    // Create a new User
    app.post('/user', users.create)

    // Retrieve all Users
    app.get('/user', users.findAll)

    // Retrieve a single User with userId
    app.get('/user/:name', users.findByName)
}
