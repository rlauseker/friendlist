import {Request, Response} from 'express'
import {User} from './user.model'
import {sequelize} from '../../database'
import {Op} from 'sequelize'

// Create and Save a new User
export async function create(req: Request, res: Response) {
    // Validate request
    if (!req.body.name) {
        return res.status(400).send({
            message: 'User content can not be empty'
        })
    }

    // Save User in the database
    const user = await User.create({
        name: req.body.name,
        email: req.body.email,
        image: req.body.image
    })

    return res.send(user)
}

// Retrieve and return all user from the database.
export async function findAll(_req: Request, res: Response) {
    const users = await User.findAll()

    if (users.length == 0) {
        return res.status(404).send({
            message: 'Users not found'
        })
    }

    return res.send(users)
}

// Find a single user with a userId
export async function findByName(req: Request, res: Response) {
    const users = await User.findAll({where: {name: {[Op.like]: '%' + req.params.name + '%'}}})

    if (!users) {
        return res.status(404).send({
            message: 'User not found'
        })
    }

    return res.send(users)
}

export async function createAndPopulateUserTable() {
    await User.sync()
    const users = await User.findAll()

    // Dummy data
    if (users.length == 0) {
        await sequelize.sync()
        User.create({
            name: 'Hans',
            email: 'email1@email.com',
            image: 'https://picsum.photos/seed/1/50/50'
        })
        User.create({
            name: 'Udo',
            email: 'email2@email.com',
            image: 'https://picsum.photos/seed/2/50/50'
        })
        User.create({
            name: 'Horst',
            email: 'email3@email.com',
            image: 'https://picsum.photos/seed/3/50/50'
        })
        User.create({
            name: 'Mario',
            email: 'email4@email.com',
            image: 'https://picsum.photos/seed/4/50/50'
        })
        User.create({
            name: 'Peter',
            email: 'email5@email.com',
            image: 'https://picsum.photos/seed/5/50/50'
        })
        User.create({
            name: 'Paul',
            email: 'email6@email.com',
            image: 'https://picsum.photos/seed/6/50/50'
        })
    }

}
