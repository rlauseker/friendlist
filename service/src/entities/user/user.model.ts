import {DataTypes, Model} from 'sequelize'
import {sequelize} from '../../database'

export class User extends Model {
    name: String
    email: String
    image: String
}

User.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    image: DataTypes.STRING
}, {sequelize, modelName: 'user'})
