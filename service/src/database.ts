import {Sequelize} from 'sequelize'

const db = process.env.POSTGRES_DB || 'db'
const user = process.env.POSTGRES_USER || 'user'
const pass = process.env.POSTGRES_PASSWORD || 'pass'

export const sequelize = new Sequelize('postgres://' + user + ':' + pass + '@postgres:5432/' + db)

